package rocks.imsofa.pcs;

import com.google.gson.Gson;
import com.graphaware.nlp.ml.textrank.TextRank;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.IOUtils;
import org.apache.cxf.helpers.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Test {
    public static void main(String[] args) throws Exception {
        String content = "Note that this code only implements key phrase extraction based on \n" +
                "keyword co-occurance described in section 3 of the Mihalcea-Tarau paper.\n" +
                "This code does not yet implement the sentence extraction described in \n" +
                "section 4 of that paper.";
        List<String> keywordList = HanLP.extractKeyword(content, 5);
        System.out.println(keywordList);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://customsearch.googleapis.com/customsearch/v1?cx=844fd530988b046d6&q=javafx&key=AIzaSyCG_vwPBCAp1DZZogsuP0-yCJWBAQDxZVY")
                .build();
        Gson gson=new Gson();
        try(InputStream input=Test.class.getClassLoader().getResourceAsStream("stopwords.txt")){
            List<String> lines=IOUtils.readLines(input, "utf-8");
            for(String line : lines){
                CoreStopWordDictionary.add(line);
            }
        }
        try (Response response = client.newCall(request).execute()) {
            String json=response.body().string();
            Map map=gson.fromJson(json, Map.class);
            List<Map> items= (List) map.get("items");
            for(Map item : items){
                String title= (String) item.get("title");
                String snippet= (String) item.get("snippet");
                String link= (String) item.get("link");
                request=new Request.Builder()
                        .url(link)
                        .get()
                        .build();
                try(Response pageResponse=client.newCall(request).execute()){
                    String pageString=pageResponse.body().string();
                    pageString=pageString.replace("<code", "<!--code");
                    pageString=pageString.replace("</code>", "</code-->");
                    Document doc = Jsoup.parse(pageString);
                    System.out.println(title);
                    /*Element question=doc.select("#answers").first();
                    Elements elements=question.select("p");*/
                    Elements elements=doc.select("p");
                    StringBuilder stringBuilder=new StringBuilder();
                    elements.forEach(new Consumer<Element>() {
                        @Override
                        public void accept(Element element) {
                            stringBuilder.append(element.text()).append("\r\n");
                        }
                    });
                    System.out.println("\t"+link);
                    System.out.println("\t"+HanLP.getSummary(stringBuilder.toString(), 500));
                    System.out.println("\t"+HanLP.extractKeyword(stringBuilder.toString().toLowerCase(), 5));
//                    System.out.println(doc.body().text());
                }
//                keywordList = HanLP.extractKeyword(snippet, 5);
//
//                System.out.println("\t"+keywordList);
            }
        }
    }
}
