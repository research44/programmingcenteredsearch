package rocks.imsofa.pcs;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseResult;
import com.github.javaparser.ParserConfiguration;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ExpressionStmt;
import com.github.javaparser.resolution.declarations.ResolvedMethodDeclaration;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.reflectionmodel.ReflectionMethodDeclaration;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import org.apache.commons.io.FileUtils;
import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.JavaType;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class TestJavaParser {
    public static void main(String[] args)throws Exception {
        JavaParser javaParser=new JavaParser(new ParserConfiguration().setSymbolResolver(
                new JavaSymbolSolver(getTypeSolver())));
        String src=FileUtils.readFileToString(new File("src\\main\\java\\rocks.imsofa.pcs.TestWikipediaAPI.java"), "utf-8");
        ParseResult<CompilationUnit> parseResult=javaParser.parse(src);
        CompilationUnit unit=parseResult.getResult().get();
        List<MethodCallExpr> list=unit.findAll(MethodCallExpr.class);
        for(MethodCallExpr m : list){
            if(m.getScope().isPresent()) {
                ResolvedMethodDeclaration resolvedMethodDeclaration=m.resolve();
                System.out.println(resolvedMethodDeclaration.getQualifiedName() + ":" + m.getName());
            }
        }
//        ParseResult<BlockStmt> parseResult=javaParser.parseBlock("{List list=new ArrayList();list.add(\"123\");}");
//        BlockStmt blockStmt=parseResult.getResult().get();
//        Node rootNode=blockStmt.findRootNode();
//        List<Node> childNodes=rootNode.getChildNodes();
//        for(Node node : childNodes){
//            ExpressionStmt expressionStmt=(ExpressionStmt)node;
//            System.out.println(expressionStmt);
//            List<MethodCallExpr> list=expressionStmt.findAll(MethodCallExpr.class);
//            for(MethodCallExpr n :list){
//                System.out.println(n.getName());
//            }
//        }
    }

    private static TypeSolver getTypeSolver() throws IOException {
        CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
        combinedTypeSolver.add(new ReflectionTypeSolver(false));
        combinedTypeSolver.add(new JavaParserTypeSolver(new File("src\\main\\java")));
        return combinedTypeSolver;
    }
}
