package rocks.imsofa.pcs;

import com.hankcs.hanlp.HanLP;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class TestStackoverflowCodeSection {
    public static void main(String[] args) throws Exception{
        String url="https://stackoverflow.com/questions/73856974/is-there-a-way-to-map-an-observablelist-of-objects-to-an-observablelist-of-a-sin";
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1) // http 1.1
                .build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();
        HttpResponse<String> response = httpClient.send(
                request, HttpResponse.BodyHandlers.ofString());
        String html=response.body();
        Document doc = Jsoup.parse(html);

        StringBuilder article=new StringBuilder();
        Elements elements=doc.select(".js-post-body");
        elements.forEach(new Consumer<Element>() {
            @Override
            public void accept(Element element) {
                Elements children=element.children();
                children.forEach(new Consumer<Element>() {
                    @Override
                    public void accept(Element element) {
                        Elements codes=element.select("code");
                        if(codes.isEmpty()){
                            article.append(element.text()).append("\r\n");
                        }
                    }
                });
            }
        });
        elements=doc.select(".js-answer");
        elements.forEach(new Consumer<Element>() {
            @Override
            public void accept(Element element) {
                article.append(element.text()).append("\r\n");
            }
        });
        System.out.println(article);
        List<String> keywords=HanLP.extractKeyword(article.toString(), 10);
        System.out.println(Arrays.deepToString(keywords.toArray()));
//        elements=doc.select("code");
//        elements.forEach(new Consumer<Element>() {
//            @Override
//            public void accept(Element element) {
//                String code=element.text();
//                List<String> keywords=HanLP.extractKeyword(code, 5);
//                System.out.println(Arrays.deepToString(keywords.toArray()));
//            }
//        });
    }
}
