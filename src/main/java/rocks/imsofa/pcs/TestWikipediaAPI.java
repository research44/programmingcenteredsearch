package rocks.imsofa.pcs;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Map;
import java.util.function.Consumer;

public class TestWikipediaAPI {
    public static void main(String[] args) throws IOException, InterruptedException {
        String html = getCategoryPageHtml("tea");
        Gson gson=new Gson();
        Map<String, Map> map=gson.fromJson(html, Map.class);
        if(map.get("parse")==null || map.get("parse").get("text")==null){
            System.out.println("category not found");
            System.exit(0);
        }
        Document doc = Jsoup.parse(map.get("parse").get("text").toString());
        Elements elements=doc.select(".mw-headline");
        elements.forEach(new Consumer<Element>() {
            @Override
            public void accept(Element element) {
                System.out.println(element.text());
                Elements aElements=element.parent().nextElementSibling().select("a");
                aElements.forEach(new Consumer<Element>() {
                    @Override
                    public void accept(Element element) {
                        System.out.println("\t"+element.text()+":https://en.wikipedia.org"+element.attr("href"));
                    }
                });
//                System.out.println("\t"+element.parent().nextElementSibling());
            }
        });
    }
    private static String getCategoryPageHtml(String keyword) throws IOException, InterruptedException {
        String propPageURL1="https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&ppprop=disambiguation&redirects&format=xml&titles="+keyword;
        String html=getHtml( propPageURL1);
        if(html.indexOf("disambiguation")!=-1){
            return getHtml("https://en.wikipedia.org/w/api.php?action=parse&page="+keyword+"&prop=text&format=json");
        }else{
            return getHtml("https://en.wikipedia.org/w/api.php?action=parse&page="+keyword+"_(disambiguation)&prop=text&format=json");
        }
    }
    private static String getHtml(String url) throws IOException, InterruptedException {
        System.out.println("load url: "+url);
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1) // http 1.1
                .build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();
        HttpResponse<String> response = httpClient.send(
                request, HttpResponse.BodyHandlers.ofString());
        String html=response.body();
        return html;
    }
}
